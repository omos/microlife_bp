# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Ondrej Mosnacek <omosnacek@gmail.com>

from datetime import datetime, tzinfo
from typing import TextIO

# https://pypi.org/project/hidapi/
import hid

CMD_READ_DATA = 0
CMD_CLEAR_DATA = 3
CMD_DISCONNECT_WITH_BPM = 4
CMD_READ_USER_DATA = 5
CMD_WRITE_ID = 6
CMD_WRITE_DEVICE_ID_AND_ERASE = 10
CMD_READ_INFO = 11
CMD_READ_DATETIME = 12
CMD_SYNC_DATETIME = 13
CMD_READ_WRITE_SERIAL_NUMBER = 15

MAGIC_ACK  = 0x81
MAGIC_NACK = 0x91

def from_be(data: list[int]) -> int:
    res = 0
    for b in data:
        res <<= 8
        res |= b
    return res

class Info:
    def __init__(self, data: list[int]) -> None:
        self._id: str = ':'.join(f'{b:02x}' for b in data[2:8])
        self._conn_type: int = data[8]
        self._measurement_count: int = from_be(data[10:13])
        self._err1: int = from_be(data[14:16])
        self._err2: int = from_be(data[17:19])
        self._err3: int = from_be(data[20:22])
        self._err5: int = from_be(data[23:25])

    def dump(self, out: TextIO, indent: str = ''):
        out.write(f'{indent}ID: {self._id}\n')
        out.write(f'{indent}Connected type: {self._conn_type}\n')
        out.write(f'{indent}Measurement count: {self._measurement_count}\n')
        out.write(f'{indent}Error 1 count: {self._err1}\n')
        out.write(f'{indent}Error 2 count: {self._err2}\n')
        out.write(f'{indent}Error 3 count: {self._err3}\n')
        out.write(f'{indent}Error 5 count: {self._err5}\n')

class UserInfo:
    def __init__(self, data: list[int]) -> None:
        self._id: str = ''.join(map(chr, data[:20]))
        self._age: int = data[20]

    def dump(self, out: TextIO, indent: str = ''):
        out.write(f'{indent}ID: {self._id}\n')
        out.write(f'{indent}Age: {self._age}\n')

class UserData:
    def __init__(self, data: list[int]) -> None:
        self._user_index: int = data[1]
        self._user1 = UserInfo(data[2:23])
        self._user2 = UserInfo(data[23:44])
        self._fw_ver: str = ''.join(map(chr, data[44:47])) + ''.join(f'{b:02}' for b in data[47:50])
        self._total_users: int = data[50]
        self._ihb, self._normal_affib, self._mam, self._ambient, self._single_cycle_affib, self._reserved, self._tubeless, self._device_id = (bool(data[52] & (1 << i)) for i in range(8))
        self._battery: int = data[53] / 10
        self._protocol_id: int = from_be(data[54:56])
        self._arrythmia_type: int = data[56]
        self._curr_mode: int = data[57]

    def dump(self, out: TextIO, indent: str = ''):
        out.write(f'{indent}Active user index: {self._user_index}\n')
        out.write(f'{indent}User 1:\n')
        self._user1.dump(out, indent + '  ')
        out.write(f'{indent}User 2:\n')
        self._user2.dump(out, indent + '  ')
        out.write(f'{indent}Firmware version: {self._fw_ver}\n')
        out.write(f'{indent}Total users: {self._total_users}\n')
        out.write(f'{indent}Irregular heartbeat: {self._ihb}\n')
        out.write(f'{indent}Normal Afib: {self._normal_affib}\n')
        out.write(f'{indent}MAM: {self._mam}\n')
        out.write(f'{indent}Ambient: {self._ambient}\n')
        out.write(f'{indent}Single-cycle Afib: {self._single_cycle_affib}\n')
        out.write(f'{indent}Tubeless: {self._tubeless}\n')
        out.write(f'{indent}Device ID: {self._device_id}\n')
        out.write(f'{indent}Battery: {self._battery} V\n')
        out.write(f'{indent}Protocol ID: {self._protocol_id}\n')
        if self._arrythmia_type == 1:
            arr_name = 'Irregular heartbeat'
        if self._arrythmia_type == 2:
            arr_name = 'PAD'
        else:
            arr_name = 'None'
        out.write(f'{indent}Arrythmia: {arr_name}\n')

class Measurement:
    def __init__(self, data: list[int], tz: tzinfo = None) -> None:
        self._systole, self._diastole, self._pulse = data[0:3]
        if data[3:8] == [19, 0, 0, 0, 0]:
            self._datetime = None
        else:
            self._datetime: datetime = datetime(2000 + data[3], data[4],
                                                data[5], data[6], data[7],
                                                tzinfo=tz)
        self._trend: int = (data[8] >> 0) & 0b11
        self._mode_afib, self._mode_single, self._afib, self._ihb, self._cuff_ok = (bool(data[8] & (1 << i)) for i in range(3, 8))

    @property
    def systole(self) -> int:
        return self._systole

    @property
    def diastole(self) -> int:
        return self._diastole

    @property
    def pulse(self) -> int:
        return self._pulse

    @property
    def timestamp(self) -> datetime:
        return self._datetime

    @property
    def trend(self) -> str:
        return ['Need Data', 'Up', 'Down', 'Normal'][self._trend]

    @property
    def mode_single(self) -> str:
        return self._mode_single

    @property
    def mode_afib(self) -> bool:
        return self._mode_afib

    @property
    def afib(self) -> bool:
        return self._afib

    @property
    def ihb(self) -> bool:
        return self._ihb

    @property
    def cuff_ok(self) -> bool:
        return self._cuff_ok

    def dump(self, out: TextIO, indent: str = ''):
        out.write(f'{indent}Systole: {self._systole}\n')
        out.write(f'{indent}Diastole: {self._diastole}\n')
        out.write(f'{indent}Pulse: {self._pulse}\n')
        out.write(f'{indent}Timestamp: {self._datetime}\n')
        out.write(f'{indent}Trend: {self._trend}\n')
        out.write(f'{indent}Mode: {self._mode_single}\n')
        out.write(f'{indent}Mode Afib: {self._mode_afib}\n')
        out.write(f'{indent}Arterial fibrilation: {self._afib}\n')
        out.write(f'{indent}Irregular heartbeat: {self._ihb}\n')
        out.write(f'{indent}Cuff OK: {self._cuff_ok}\n')

class Device:
    def __init__(self, device: hid.device, tz: tzinfo = None) -> None:
        self._device: hid.device = device
        self._tz: tzinfo = tz

        self._device.send_feature_report([0, 1, 128, 37, 0, 0, 0, 0, 8])

    def close(self) -> None:
        self._device.close()

    def _send_command(self, cmd_id: int, arg: list[int]) -> None:
        payload = [0x4d, 0xff, 0, len(arg) + 2, cmd_id] + arg
        cmd = [0, len(arg) + 6] + payload + [sum(payload) & 0xff]
        self._device.write(cmd)

    def _read_packet(self) -> list[int]:
        while True:
            res = self._device.read(33, timeout_ms=100)
            if len(res):
                return res[1:1+res[0]]

    def _read_response(self) -> list[int]:
        length = 0
        payload = []
        while len(payload) < 4:
            res = self._read_packet()
            payload += res

        assert payload[:2] == [0x4d, 0x3a]
        length = from_be(payload[2:4])

        while len(payload) - 4 != length:
            res = self._read_packet()
            payload += res

        assert (sum(payload[:-1]) & 0xff) == payload[-1]
        return payload[4:-1]

    def read_data(self, patient_id: int) -> list[Measurement]:
        self._send_command(CMD_READ_DATA, [0, 0, 0, 0, 0, 0, patient_id])
        res = self._read_response()
        if res == [MAGIC_NACK]:
            return None
        return [Measurement(res[pos:pos+10], self._tz)
                for pos in range(38, len(res), 10)]

    def export_data_to_csv(self, out: TextIO, sep: str) -> None:
        out.write(f'User{sep}Time{sep}Systole{sep}Diastole{sep}Pulse{sep}Trend{sep}Mode{sep}Mode Afib{sep}Arterial fibrilation{sep}Irregular heartbeat{sep}Cuff OK\n')
        for i in (1, 2):
            measurements = self.read_data(i)
            for m in measurements:
                ts = f'{m.timestamp:%x %X}' if m.timestamp != None else ''
                out.write(f'{i}{sep}{ts}{sep}{m.systole}{sep}{m.diastole}{sep}{m.pulse}{sep}{m.trend}{sep}{m.mode_single}{sep}{m.mode_afib}{sep}{m.afib}{sep}{m.ihb}{sep}{m.cuff_ok}\n')

    def read_datetime(self) -> datetime:
        self._send_command(CMD_READ_DATETIME, [])
        res = self._read_response()
        if res[1] != 1:
            return None
        return datetime(2000 + res[2], res[3], res[4], res[5], res[6],
                        res[7], tzinfo=self._tz)

    def read_info(self) -> Info:
        self._send_command(CMD_READ_INFO, [])
        return Info(self._read_response())

    def read_user_data(self) -> UserData:
        self._send_command(CMD_READ_USER_DATA, [])
        return UserData(self._read_response())

    def read_serial_number(self) -> str:
        self._send_command(CMD_READ_WRITE_SERIAL_NUMBER, [0])
        return ''.join(map(chr, self._read_response()[2:])).split(' ')[0]

    def clear_data(self, patient_id: int) -> None:
        self._send_command(CMD_CLEAR_DATA, [patient_id])
        #FIXME ack/nack?
        print(self._read_response())

    def set_datetime(self, dt: datetime) -> None:
        assert isinstance(dt, datetime)
        dt = dt.astimezone(self._tz)
        data = [dt.year % 100, dt.month, dt.day, dt.hour, dt.minute,
                dt.second]
        self._send_command(CMD_SYNC_DATETIME, data)
        # FIXME throw a reasonable exception
        assert self._read_response() == [MAGIC_ACK]

    def write_device_id_and_erase(self, device_id: list[int]) -> None:
        assert len(device_id) == 6
        self._send_command(CMD_WRITE_DEVICE_ID_AND_ERASE, device_id)
        #FIXME ack/nack?
        print(self._read_response())

    def write_id(self, user_index: int, user_id: str, age: int) -> None:
        assert len(user_id) <= 20
        # pad with spaces
        user_id = [ord(c) for c in user_id] + [0x20] * (20 - len(user_id))
        self._send_command(CMD_WRITE_ID, [user_index] + user_id + [age])
        #FIXME ack/nack?
        print(self._read_response())

    def write_serial_number(self, sn: str) -> None:
        assert len(sn) <= 20
        # pad with spaces
        sn = [ord(c) for c in sn] + [0x20] * (20 - len(user_id))
        self._send_command(CMD_READ_WRITE_SERIAL_NUMBER, [0x01, 0x57, 0x53, 0x4e] + sn)
        #FIXME ack/nack?
        print(self._read_response())
