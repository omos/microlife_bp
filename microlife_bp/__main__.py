# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Ondrej Mosnacek <omosnacek@gmail.com>

from . import list_devices

import locale
import sys

from argparse import ArgumentParser
from datetime import datetime

locale.setlocale(locale.LC_ALL, '')

int_hex = lambda x: int(x, base=16)

parser = ArgumentParser(prog='microlife',
                        description='A CLI for working with Microlife blood pressure monitors')
parser.add_argument('-f', '--first', action='store_true',
                    help='use the first matching device when more than one are found')
parser.add_argument('-t', '--usb-path', metavar='USB_PATH',
                    help='use device with the specified USB path')
parser.add_argument('-v', '--vendor-id', metavar='VENDOR_ID', type=int_hex,
                    help='filter devices by the given Vendor ID (in hex format)')
parser.add_argument('-p', '--product-id', metavar='PRODUCT_ID', type=int_hex,
                    help='filter devices by the given Product ID (in hex format)')
parser.add_argument('-s', '--serial-number', metavar='SERIAL_NUM', type=str,
                    help='filter devices by the given serial number')

subparsers = parser.add_subparsers()

parser_list = subparsers.add_parser('list',
                                    help='list available devices')
parser_list.set_defaults(command='list')

parser_dt = subparsers.add_parser('dump-time',
                                  help='print device time in a human-readable form')
parser_dt.set_defaults(command='dump-time')

parser_di = subparsers.add_parser('dump-info',
                                  help='print device info in a human-readable form')
parser_di.set_defaults(command='dump-info')

parser_du = subparsers.add_parser('dump-users',
                                  help='print user info in a human-readable form')
parser_du.set_defaults(command='dump-users')

parser_dm = subparsers.add_parser('dump-measurements',
                                  help='print all measurements in a human-readable form')
parser_dm.set_defaults(command='dump-measurements')
parser_dm.add_argument('user_index', metavar='USER_INDEX',
                       help='User index', type=int, choices=[1,2],
                       default=1)

parser_csv = subparsers.add_parser('export-csv',
                                   help='export all measurements in the CSV format')
parser_csv.set_defaults(command='export-csv')
parser_csv.add_argument('--tsv', action='store_true', help='Use tab as sepatator instead of a comma')
parser_csv.add_argument('-o', '--output', metavar='FILE',
                        help='Output file', default='-')

parser_csv = subparsers.add_parser('sync-time',
                                   help='sync device time with computer time')
parser_csv.set_defaults(command='sync-time')

def main() -> int:
    args = parser.parse_args()

    def filter_devices():
        for dev in list_devices():
            if args.usb_path != None and dev.path.decode() != args.usb_path:
                continue
            if args.vendor_id != None and dev.vendor_id != args.vendor_id:
                continue
            if args.product_id != None and dev.product_id != args.product_id:
                continue
            if args.serial_number != None:
                with dev.open_context() as odev:
                    if odev.read_serial_number() != args.serial_number:
                        continue
            yield dev

    matching_devices = list(filter_devices())

    if args.command == 'list':
        for dev in matching_devices:
            with dev.open_context() as odev:
                print(f'path={dev.path.decode()} vendor_id={dev.vendor_id:04x} product_id={dev.product_id:04x} sn={odev.read_serial_number()}')
        return 0

    if len(matching_devices) == 0:
        sys.stderr.write('error: no matching device found!\n')
        return 1

    if not args.first and len(matching_devices) != 1:
        sys.stderr.write('error: more than one matching device found!\n')
        return 1

    with matching_devices[0].open_context() as device:
        if args.command == 'dump-time':
            print(device.read_datetime())
        elif args.command == 'dump-info':
            print('Device info:')
            device.read_info().dump(sys.stdout, '  ')
        elif args.command == 'dump-users':
            print('User data:')
            device.read_user_data().dump(sys.stdout, '  ')
        elif args.command == 'dump-measurements':
            print(f'Measurements for user #{args.user_index}:')
            m = device.read_data(args.user_index)
            for i in range(len(m)):
                print(f'  Measurement #{i}:')
                m[i].dump(sys.stdout, '    ')
        elif args.command == 'export-csv':
            sep = '\t' if args.tsv else ','
            if args.output == '-':
                device.export_data_to_csv(sys.stdout, sep)
            else:
                with open(args.output, 'w') as f:
                    device.export_data_to_csv(f, sep)
        elif args.command == 'sync-time':
            device.set_datetime(datetime.now())
    return 0

sys.exit(main())
