# microlife_bp - a Python library for communicating with Microlife blood pressure monitors

Disclaimer: While I'm publishing this code under an open-source license, please be aware that it has been written based on studying decompiled code from the [official BPA+ application from Microlife](https://www.microlife.com/technologies/bp-analyzer/bpa-download-and-install), so its distribution and/or use may not be formally legal.

This repository provides a library (`import microlife_bp`) and a basic CLI (`python3 -m microlife_bp`) for interacting with Microlife blood pressure monitors. Currently only the "4G" devices are supported (USB vendor ID `0x04d9`).

# Dependencies

Aside from modern Python 3, this library requires the `hidapi` package providing the module `hid` ([PyPI link](https://pypi.org/project/hidapi/)).

# Usage

## CLI

TBD - for now use `--help` to see the list of supported commands.

## Library

Basic usage:

```
import microlife_bp

devices = list(microlife_bp.list_devices())

with devices[0].open_context() as device:
    print(device.read_datetime())
    help(device)
```
