# SPDX-License-Identifier: MIT
# SPDX-FileCopyrightText: 2023 Ondrej Mosnacek <omosnacek@gmail.com>

from . import bp4g

from contextlib import contextmanager
from datetime import tzinfo
from typing import Iterator

# https://pypi.org/project/hidapi/
import hid

from typing import Dict

VENDOR_ID_4G = 0x04d9

class Device:
    def __init__(self, ddict: Dict) -> None:
        self._path: bytes = ddict['path']
        self._product_id: int = ddict['product_id']

    @property
    def path(self) -> bytes:
        return self._path

    @property
    def vendor_id(self) -> int:
        return VENDOR_ID_4G

    @property
    def product_id(self) -> int:
        return self._product_id

    def open(self, tzinfo: tzinfo = None) -> bp4g.Device:
        device = hid.device()
        device.open_path(self._path)
        return bp4g.Device(device, tzinfo)

    @contextmanager
    def open_context(self, tzinfo: tzinfo = None) -> Iterator[bp4g.Device]:
        device = self.open(tzinfo)
        try:
            yield device
        finally:
            device.close()

def list_devices() -> Iterator[Device]:
    for device in hid.enumerate(vendor_id=VENDOR_ID_4G):
        yield Device(device)
